import cdsapi

c = cdsapi.Client(timeout=600,quiet=False,debug=True)

MONTHS = [
 "01", "02", "03", "04", "05", "06",
 "07", "08", "09", "10", "11", "12"
]
for month in MONTHS:
 result = c.service(
 "tool.toolbox.orchestrator.workflow", 
 params={
 "realm": "wbbjmu",
 "project": "brazil", 
 "version": "master", 
 "kwargs": {
 "dataset": "reanalysis-era5-single-levels",
 "product_type": "reanalysis",
 "variable": "2m_temperature",
 "statistic": "daily_mean",
 "year": "2000",
 "month": month,
 "time_zone": "UTC-03:00",
 "frequency": "1-hourly",
 "grid": "0.25/0.25",
 "area": {"lat": [-34, 5.4], "lon": [-75, -27]}
 },
 "workflow_name": "application"
 })

 c.download(result)
